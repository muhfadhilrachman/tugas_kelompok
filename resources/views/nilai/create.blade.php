@extends('template')

@section('content')
<div class="form col-6 m-auto">
    <form action="">
        <div class="form-group mt-3">
            <label for="">Nama Siswa</label>
            <select name="" class="form-control" id="">
                <option value="">Nama Mahasiswa</option>
                <option value="">Ajis</option>
                <option value="">Padil</option>
                <option value="">dede</option>
                <option value="">ganjar</option>
                <option value="">anjas</option>
            </select>
        </div>
        <div class="form-group mt-3">
            <label for="">Mata Kuliah</label>
            <select name="" class="form-control" id="">
                <option value="">Mata Kuliah</option>
                <option value="">Sistem Informasi</option>
                <option value="">Peternakan</option>
                <option value="">Pertanian</option>
                <option value="">Ekonomi</option>
                <option value="">Peranakan</option>
            </select>
        </div>
        <div class="form-group mt-3">
            <label for="">SKS </label>
            <input type="number" class="form-control">
        </div>
        <div class="form-group mt-3">
            <label for="">Semester</label>
            <select name="" class="form-control" id="">
                <option value="">1</option>
                <option value=""> 2</option>
                <option value="">3</option>
                <option value="">4</option>
                <option value="">5</option>
                <option value="">6</option>
            </select>
        </div>
       
           
        <div class="form-group mt-3">
            <label for="">Nilai Tugas </label>
            <input type="number" class="form-control">
        </div>
           
        <div class="form-group mt-3">
            <label for="">Nilai Kehadiran </label>
            <input type="number" class="form-control">
        </div>
        <div class="form-group mt-3">
            <label for="">Nilai UTS </label>
            <input type="number" class="form-control">
        </div>
        <div class="form-group mt-3">
            <label for="">Nilai UAS </label>
            <input type="text" class="form-control">
        </div>
        <div class="form-group mt-3">
            <label for="">Nilai Akhir </label>
            <input type="text" class="form-control">
        </div>

       <div class="mt-5 mb-5">
        <a class="btn btn-danger" href="/nilai">Back</a>
        <button class="btn btn-primary">Submit</button>
       </div>
    </form>
</div>
@endsection