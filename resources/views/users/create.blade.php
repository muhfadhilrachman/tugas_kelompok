@extends('template')



@section('input-search')
<input type="text" class="form-control bg-light border-0 small" placeholder="cari user..."
aria-label="Search" aria-describedby="basic-addon2">
@endsection


@section('content')
    
<div class="form col-6 m-auto">
    <form action="">
        <div class="form-group mt-3">
            <label for="">Nama </label>
            <input type="text" class="form-control">
        </div>
        <div class="form-group mt-3">
            <label for="">Email </label>
            <input type="email" class="form-control">
        </div>
        <div class="form-group mt-3">
            <label for="">Password </label>
            <input type="password" class="form-control">
        </div>
        <div class="form-group mt-3">
            <label for="">Confirm Password </label>
            <input type="password" class="form-control">
        </div>
        <div class="form-group mt-3">
            <label for="">Alamat </label>
            <input type="text" class="form-control">
        </div>
        <div class="form-group mt-3">
            <label for="">Status </label>
            {{-- <input type="text" class="form-control"> --}}
            <select name="" class="form-control" id="">
                <option value="">Mahasiswa</option>
                <option value="">Dosen</option>
            </select>
        </div>
       <div class="form-group">
        <label for="">Gambar </label>

        <div class="custom-file">
            <input type="file" class="custom-file-input" id="customFile">
            <label class="custom-file-label" for="customFile">gundulmu</label>
          </div>
       </div>

       <div class="mt-5 mb-5">
        <a class="btn btn-danger" href="/">Back</a>
        <button class="btn btn-primary">Submit</button>
       </div>
    </form>
</div>
@endsection
