<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// USER

Route::get('/', function () {
    return view('users.list',["title"=>"Users"]);
});

Route::get('/create-user', function(){
    return view('users.create',["title"=>"Create User"]);
});


//


// MATKUL




//


// SEMESTER



//


// NILAI


Route::get('/nilai',function(){
    return view('nilai.list',['title'=>'Nilai']);
});

Route::get('/create-nilai',function(){
    return view('nilai.create',['title'=>'Create Nilai']);
});

//


// JURUSAN 

//



